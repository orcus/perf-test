/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "matrix_mul.hpp"
#include "env.hpp"

#include <boost/program_options.hpp>
#include <iostream>
#include <ostream>
#include <string>
#include <chrono>

namespace po = boost::program_options;

using std::cout;
using std::cerr;
using std::endl;

namespace {

class stack_printer
{
public:
    explicit stack_printer(const char* msg) :
        m_msg(msg)
    {
        std::cout << m_msg << ": --begin" << std::endl;
        m_start_time = get_time();
    }

    ~stack_printer()
    {
        double end_time = get_time();
        std::cout << m_msg << ": --end (duration: " << (end_time-m_start_time) << " sec)" << std::endl;
    }

    void print_time(int line) const
    {
        double end_time = get_time();
        std::cout << m_msg << ": --(" << line << ") (duration: " << (end_time-m_start_time) << " sec)" << std::endl;
    }

private:
    double get_time() const
    {
        unsigned long usec_since_epoch =
            std::chrono::duration_cast<std::chrono::microseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();

        return usec_since_epoch / 1000000.0;
    }

    std::string m_msg;
    double m_start_time;
};

void print_usage(std::ostream& os, const po::options_description& desc)
{
    os << "Usage: matrix-mul [OPTIONS]" << endl << endl;
    os << desc;
}

} // anonymous namespace

int main(int argc, char** argv)
{
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Print this help.")
        ("element-size,n", po::value<size_t>(), "Element size.")
        ("alignment,a", po::value<size_t>(), "Alignment size.")
        ("func-type,t", po::value<std::string>(), "Function type.")
    ;

    po::positional_options_description po_desc;
    po_desc.add("input", 1);

    po::options_description cmd_opt;
    cmd_opt.add(desc);

    po::variables_map vm;
    try
    {
        po::store(
            po::command_line_parser(argc, argv).options(cmd_opt).positional(po_desc).run(), vm);
        po::notify(vm);
    }
    catch (const std::exception& e)
    {
        // Unknown options.
        cerr << e.what() << endl;
        print_usage(cout, desc);
        return EXIT_FAILURE;
    }

    if (vm.count("help"))
    {
        print_usage(cout, desc);
        return EXIT_SUCCESS;
    }

    if (!vm.count("element-size"))
    {
        cerr << "Element size is required but is not given." << endl;
        print_usage(cout, desc);
        return EXIT_FAILURE;
    }

    if (!vm.count("func-type"))
    {
        cerr << "Function type is required but is not given." << endl;
        print_usage(cout, desc);
        return EXIT_FAILURE;
    }

    const size_t alignment = vm.count("alignment") > 0 ? vm["alignment"].as<size_t>() : 8u;

    const size_t elem_size = vm["element-size"].as<size_t>();
    const std::string func_type = vm["func-type"].as<std::string>();
    cout << "element size: " << elem_size << endl;
    cout << "alignment: " << alignment << endl;
    cout << "function type: " << func_type << endl;
    cout << "L1 cacheline size: " << L1_CACHE_LINESIZE << endl;

    context cxt;
    cxt.alignment = alignment;
    cxt.size = elem_size;

    {
        stack_printer __stack_printer__("array creation with filled inputs");
        cxt.input_l = create_aligned_square_array(cxt.alignment, cxt.size, true);
        cxt.input_r = create_aligned_square_array(cxt.alignment, cxt.size, true);
        cxt.output = create_aligned_square_array(cxt.alignment, cxt.size, false);
    }

    auto print_array = [&cxt](const double* p, const char* msg)
    {
        cout << "input " << msg << ':' << endl;
        cout << "  - memory size: " << (sizeof(double) * cxt.size * cxt.size) << endl;
        cout << "  - memory address: " << std::hex << p << endl;
        auto rem = std::uintptr_t(p) % cxt.alignment;
        cout << "  - aligned by " << std::dec << cxt.alignment << ": " << (rem ? "no" : "yes") << endl;
    };

    print_array(cxt.input_l.get(), "left");
    print_array(cxt.input_r.get(), "right");
    print_array(cxt.output.get(), "output");

    if (func_type == "baseline")
    {
        stack_printer __stack_printer__("baseline");
        matrix_mul_baseline(cxt);
    }
    else if (func_type == "transpose")
    {
        stack_printer __stack_printer__("transpose");
        matrix_mul_transpose(cxt);
    }
    else if (func_type == "submatrix")
    {
        stack_printer __stack_printer__("submatrix");
        matrix_mul_submatrix(cxt);
    }
    else if (func_type == "submatrix-sse2")
    {
        stack_printer __stack_printer__("submatrix-sse2");
        matrix_mul_submatrix_sse2(cxt);
    }
#ifdef __AVX__
    else if (func_type == "submatrix-avx")
    {
        stack_printer __stack_printer__("submatrix-avx");
        matrix_mul_submatrix_avx(cxt);
    }
#endif
    else if (func_type == "all")
    {
        {
            stack_printer __stack_printer__("baseline");
            matrix_mul_baseline(cxt);
        }
        {
            stack_printer __stack_printer__("transpose");
            matrix_mul_transpose(cxt);
        }
        {
            stack_printer __stack_printer__("submatrix");
            matrix_mul_submatrix(cxt);
        }
        {
            stack_printer __stack_printer__("submatrix-sse2");
            matrix_mul_submatrix_sse2(cxt);
        }
#ifdef __AVX__
        {
            stack_printer __stack_printer__("submatrix-avx");
            matrix_mul_submatrix_avx(cxt);
        }
#endif
    }
    else
    {
        cerr << "invalid function type" << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
