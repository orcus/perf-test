/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <iostream>
#include <algorithm>
#include <hwloc.h>

using std::cout;
using std::cerr;
using std::endl;

int main(int argc, char** argv)
{
    hwloc_topology_t topo;

    if (hwloc_topology_init(&topo) < 0)
    {
        cerr << "failed to initialize hwloc." << endl;
        std::exit(1);
    }

    if (hwloc_topology_load(topo) < 0)
    {
        cerr << "failed to load topology." << endl;
        std::exit(1);
    }

    int topo_depth = hwloc_topology_get_depth(topo);
    cout << "topology depth: " << topo_depth << endl;

    for (int depth = 0; depth < topo_depth; ++depth)
    {
        int n_objs = hwloc_get_nbobjs_by_depth(topo, depth);
        cout << "* depth " << depth << ": number of objects = " << n_objs << endl;

        for (int i = 0; i < n_objs; ++i)
        {
            hwloc_obj_t obj = hwloc_get_obj_by_depth(topo, depth, i);

            std::string obj_type(512, '\0');
            int n = hwloc_obj_type_snprintf(obj_type.data(), obj_type.size(), obj, 1);
            obj_type.resize(n);

            cout << "  * object " << i << ": " << obj_type << endl;

            std::string obj_attr(512, '\0');
            n = hwloc_obj_attr_snprintf(obj_attr.data(), obj_attr.size(), obj, "\n", 1);
            obj_attr.resize(n);

            const char* p = obj_attr.data();
            const char* p_end = p + obj_attr.size();
            const char* p0 = nullptr;

            for (; p != p_end; ++p)
            {
                if (!p0)
                    p0 = p;

                if (*p == '\n')
                {
                    cout << "    * " << std::string(p0, p - p0) << endl;
                    p0 = nullptr;
                }
            }

            if (p0)
                cout << "    * " << std::string(p0, p - p0) << endl;
        }
    }

    hwloc_topology_destroy(topo);

    return EXIT_SUCCESS;
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */

