/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <orcus/stream.hpp>
#include <orcus/dom_tree.hpp>
#include <orcus/xml_namespace.hpp>
#include <orcus/xml_structure_tree.hpp>
#include <orcus/sax_ns_parser.hpp>
#include <orcus/measurement.hpp>
#include <orcus/string_pool.hpp>
#include <mdds/rtree.hpp>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/interprocess/file_mapping.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include <iostream>
#include <exception>
#include <unordered_map>
#include <fstream>
#include <chrono>

namespace {

class stack_printer
{
public:
    explicit stack_printer(const char* msg, double& duration) :
        m_msg(msg),
        m_duration(duration)
    {
        std::cerr << m_msg << ": --begin" << std::endl;
        m_start_time = get_time();
    }

    ~stack_printer()
    {
        double end_time = get_time();
        m_duration = end_time - m_start_time;
        std::cerr << m_msg << ": --end (duration: " << m_duration << " sec)" << std::endl;
    }

private:
    double get_time() const
    {
        double v = std::chrono::system_clock::now().time_since_epoch() / std::chrono::milliseconds(1);
        return v / 1000.0;
    }

    std::string m_msg;
    double m_start_time;
    double& m_duration;
};

}

using namespace std;
namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace bip = boost::interprocess;
namespace dom = orcus::dom;

class invalid_osm_structure : public std::exception
{
    std::string m_msg;
public:
    invalid_osm_structure(const std::string& msg) : m_msg(msg) {}

    virtual const char* what() const noexcept override
    {
        return m_msg.data();
    }
};

struct point
{
    double lat;
    double lon;

    point() : lat(0.0), lon(0.0) {}
    point(double _lat, double _lon) : lat(_lat), lon(_lon) {}

    void reset()
    {
        lat = 0.0;
        lon = 0.0;
    }
};

struct shape
{
    std::vector<point> points;

    point tl;
    point br;

    shape() = default;
    shape(shape&& other) : points(std::move(other.points)), tl(other.tl), br(other.br) {}

    void calc_bounding_box()
    {
        if (points.empty())
            return;

        auto it = points.begin();
        tl = br = *it;
        for (++it; it != points.end(); ++it)
        {
            tl.lat = std::min(tl.lat, it->lat);
            tl.lon = std::min(tl.lon, it->lon);
            br.lat = std::max(br.lat, it->lat);
            br.lon = std::max(br.lon, it->lon);
        }

        if (tl.lat > br.lat || tl.lon > br.lon)
            throw std::runtime_error("The bounding box calculation code is not correct!!");
    }

    void reset()
    {
        points.clear();
        tl.reset();
        br.reset();
    }
};

using rtree_type = mdds::rtree<double, shape>;

struct timings_type
{
    double osm_load    = -1.0;
    double build_rtree = -1.0;
    double rtree_size  = -1.0;
    double mass_search = -1.0;
    int search_count   = -1;
    double export_svg  = -1.0;

    void print()
    {
        cout << "-- csv table" << endl;
        cout << "osm_load,build_rtree,rtree_size,mass_search,search_count,export_svg" << endl;
        cout << osm_load << ',' << build_rtree << ',' << rtree_size << ',' << mass_search << ',' << search_count << ',' << export_svg << endl;
    }
};

class osm_data_container
{
    using points_type = std::unordered_map<long, point>;
    using shapes_type = std::unordered_map<long, shape>;
    points_type m_points;
    shapes_type m_shapes;

    struct handler_type
    {
        points_type& m_points;
        shapes_type& m_shapes;

        std::vector<orcus::pstring> m_elem_stack;
        long m_cur_id;
        long m_cur_ref;
        point m_cur_point;
        shape m_cur_shape;
        long m_cur_shape_id;
        orcus::string_pool m_sp;

        handler_type(points_type& points, shapes_type& shapes) :
            m_points(points),
            m_shapes(shapes),
            m_cur_id(-1),
            m_cur_ref(-1),
            m_cur_shape_id(-1)
        {
            m_cur_point.lat = 0.0;
            m_cur_point.lon = 0.0;
        }

        void doctype(const orcus::sax::doctype_declaration& /*dtd*/) {}

        void start_declaration(const orcus::pstring& /*decl*/) {}

        void end_declaration(const orcus::pstring& /*decl*/) {}

        void start_element(const orcus::sax_ns_parser_element& elem)
        {
            m_elem_stack.push_back(m_sp.intern(elem.name).first);

            if (elem.name == "node")
            {
                if (m_cur_id < 0)
                    throw invalid_osm_structure("current id is negative.");

                m_points.emplace(m_cur_id, m_cur_point);
                m_cur_id = -1;
                m_cur_point.reset();
            }
            else if (elem.name == "way")
            {
                m_cur_shape_id = m_cur_id;
            }
            else if (elem.name == "nd")
            {
                auto it = m_points.find(m_cur_ref);
                if (it == m_points.end())
                    throw invalid_osm_structure("way contains a missing point!");

                const point& pt = it->second;
                m_cur_shape.points.push_back(pt);
            }
        }

        void end_element(const orcus::sax_ns_parser_element& elem)
        {
            if (m_elem_stack.empty())
                throw invalid_osm_structure("element stack is empty.");

            if (m_elem_stack.back() != elem.name)
            {
                std::ostringstream os;
                os << "element named '" << m_elem_stack.back() << "' was expected, but '" << elem.name << "' is found.";
                throw invalid_osm_structure(os.str());
            }

            if (elem.name == "way")
            {
                if (m_cur_shape_id < 0)
                    throw invalid_osm_structure("current shape id is negative.");

                m_cur_shape.calc_bounding_box();
                m_shapes.emplace(m_cur_shape_id, std::move(m_cur_shape));
                m_cur_shape.reset();
                m_cur_shape_id = -1;
            }

            m_elem_stack.pop_back();
        }

        void characters(const orcus::pstring& /*val*/, bool /*transient*/) {}

        void attribute(const orcus::pstring& /*name*/, const orcus::pstring& /*val*/) {}

        void attribute(const orcus::sax_ns_parser_attribute& attr)
        {
            if (m_elem_stack.empty())
                return;

            if (attr.name == "id")
                m_cur_id = orcus::to_long(attr.value);
            else if (attr.name == "lat")
                m_cur_point.lat = orcus::to_double(attr.value);
            else if (attr.name == "lon")
                m_cur_point.lon = orcus::to_double(attr.value);
            else if (attr.name == "ref")
                m_cur_ref = orcus::to_long(attr.value);
        }
    };

    handler_type m_handler;

public:
    osm_data_container() : m_handler(m_points, m_shapes) {}

    void load(const char* p_content, size_t n_content)
    {
        orcus::xmlns_repository repo;
        orcus::xmlns_context cxt = repo.create_context();
#if 1
        orcus::sax_ns_parser<handler_type> parser(p_content, n_content, true, cxt, m_handler);
        parser.parse();
#else
        dom::document_tree dom(cxt);
        dom.load(p_content, n_content, true);

        dom::const_node root = dom.root();
        if (root.name() != dom::entity_name("osm"))
            throw invalid_osm_structure("root element must be 'osm'.");

        const dom::entity_name name_node("node");
        const dom::entity_name name_way("way");

        for (size_t i = 0, n = root.child_count(); i < n; ++i)
        {
            dom::const_node node = root.child(i);
            dom::entity_name this_name = node.name();
            if (this_name == name_node)
                append_node(node);
            else if (this_name == name_way)
                append_way(node);
        }
#endif
    }

    rtree_type build_rtree_bulkload()
    {
        rtree_type::bulk_loader tree;

        for (shapes_type::value_type& v : m_shapes)
        {
            shape& sh = v.second;
            tree.insert(
                {{sh.tl.lat, sh.tl.lon}, {sh.br.lat, sh.br.lon}},
                std::move(sh));
        }

        return tree.pack();
    }

    rtree_type build_rtree()
    {
        rtree_type tree;

        for (shapes_type::value_type& v : m_shapes)
        {
            shape& sh = v.second;
            tree.insert(
                {{sh.tl.lat, sh.tl.lon}, {sh.br.lat, sh.br.lon}},
                std::move(sh));
        }

        return tree;
    }

    void print_summary() const
    {
        cout << "number of points: " << m_points.size() << endl;
        cout << "number of shapes: " << m_shapes.size() << endl;
    }

private:

#if 0
    void append_node(const dom::const_node& node)
    {
        long id = orcus::to_long(node.attribute("id"));
        double lat = orcus::to_double(node.attribute("lat"));
        double lon = orcus::to_double(node.attribute("lon"));

        m_points.emplace(id, point(lat, lon));
    }

    void append_way(const dom::const_node& way)
    {
        long id = orcus::to_long(way.attribute("id"));

        const dom::entity_name nd_name("nd");

        shape sh;

        for (size_t i = 0, n = way.child_count(); i < n; ++i)
        {
            dom::const_node cn = way.child(i);
            if (cn.name() == nd_name)
            {
                long point_id = orcus::to_long(cn.attribute("ref"));
                auto it = m_points.find(point_id);
                if (it == m_points.end())
                    throw invalid_osm_structure("way contains a missing point!");

                const point& pt = it->second;
                sh.points.push_back(pt);
            }
        }

        sh.calc_bounding_box();
        m_shapes.emplace(id, std::move(sh));
    }
#endif
};

void parse_osm_file(
    const char* p_content, size_t n_content, const std::string& outpath, bool bulkload)
{
    timings_type timings;

    osm_data_container osm;
    {
        stack_printer __stack_printer__("load osm file", timings.osm_load);
        osm.load(p_content, n_content);
    }

    osm.print_summary();

    rtree_type rtree;

    {
        stack_printer __stack_printer__("build R-tree", timings.build_rtree);
        rtree = bulkload ? osm.build_rtree_bulkload() : osm.build_rtree();
    }

    size_t size = 0;

    {
        stack_printer __stack_printer__("rtree::size()", timings.rtree_size);
        size = rtree.size();
    }

    auto extent = rtree.extent();

    cout << "size: " << size << endl;
    cout << "extent: " << extent.to_string() << endl;

    if (!outpath.empty())
    {
        stack_printer __stack_printer__("export svg", timings.export_svg);
        std::string svg_content = rtree.export_tree(rtree_type::export_tree_type::extent_as_svg);
        std::ofstream of(outpath);
        of << svg_content;
    }

    cout << "lat range: " << extent.start.d[0] << " - " << extent.end.d[0] << endl;
    cout << "lon range: " << extent.start.d[1] << " - " << extent.end.d[1] << endl;

    int interval_ratio = 40;
    size_t res_minimum = 10;

    double lat_interval = (extent.end.d[0] - extent.start.d[0]) / interval_ratio;
    double lon_interval = (extent.end.d[1] - extent.start.d[1]) / interval_ratio;

    {
        stack_printer __stack_printer__("rtree::search()", timings.mass_search);
        timings.search_count = 0;
        for (int lat_step = 1; lat_step <= (interval_ratio-1); ++lat_step)
        {
            for (int lon_step = 1; lon_step <= (interval_ratio-1); ++lon_step)
            {
                double lat = extent.start.d[0] + lat_interval * lat_step;
                double lon = extent.start.d[1] + lon_interval * lon_step;

                auto results = rtree.search({lat, lon}, rtree_type::search_type::overlap);
                ++timings.search_count;
                size_t n_res = std::distance(results.begin(), results.end());
                if (n_res >= res_minimum)
                    cout << "search " << timings.search_count << ": (lat: " << lat << "; lon: " << lon << "; results: " << n_res << ")" << endl;
            }
        }

        cout << "number of searches performed: " << timings.search_count << " (only searches with more than " << res_minimum << " results are shown)" << endl;
    }

    timings.print();
}

void dump_xml_structure(const char* content, size_t n_content)
{
    orcus::xmlns_repository repo;
    orcus::xmlns_context cxt = repo.create_context();
    orcus::xml_structure_tree tree(cxt);
    tree.parse(content, n_content);
    tree.dump_compact(cout);
}

void dump_summary(const char* p_content, size_t n_content)
{
    osm_data_container osm;
    {
        double foo;
        stack_printer __stack_printer__("load osm file", foo);
        osm.load(p_content, n_content);
    }
    osm.print_summary();
}

void print_usage(ostream& os, const po::options_description& desc)
{
    os << "Usage: osm-load [OPTIONS] FILE" << endl << endl;
    os << desc;
}

int main(int argc, char** argv)
{
    bool bulkload = false;

    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Print this help.")
        ("dump", po::value<std::string>(), "Load the input OSM file and print the summary of its contnet.")
        ("output,o", po::value<std::string>(), "Path to a SVG output.")
        ("bulkload", po::bool_switch(&bulkload), "Whether or not to bulkload map data to R-tree.")
    ;

    po::options_description hidden("");
    hidden.add_options()
        ("input", po::value<string>(), "input file");

    po::positional_options_description po_desc;
    po_desc.add("input", 1);

    po::options_description cmd_opt;
    cmd_opt.add(desc).add(hidden);

    po::variables_map vm;
    try
    {
        po::store(
            po::command_line_parser(argc, argv).options(cmd_opt).positional(po_desc).run(), vm);
        po::notify(vm);
    }
    catch (const exception& e)
    {
        // Unknown options.
        cerr << e.what() << endl;
        print_usage(cout, desc);
        return EXIT_FAILURE;
    }

    if (vm.count("help"))
    {
        print_usage(cout, desc);
        return EXIT_FAILURE;
    }

    if (!vm.count("input"))
    {
        cerr << "No input file." << endl;
        print_usage(cout, desc);
        return EXIT_FAILURE;
    }

    std::string input_path = vm["input"].as<std::string>();

    boost::uintmax_t input_file_size = fs::file_size(input_path);
    bip::file_mapping mapped_file(input_path.data(), bip::read_only);
    bip::mapped_region region(mapped_file, bip::read_only, 0, input_file_size);

    const char* content = static_cast<const char*>(region.get_address());

    if (vm.count("dump"))
    {
        std::string dump_mode = vm["dump"].as<std::string>();
        if (dump_mode == "xml-structure")
            dump_xml_structure(content, input_file_size);
        else if (dump_mode == "summary")
            dump_summary(content, input_file_size);
        else
        {
            cerr << "Unknown dump mode" << endl;
            return EXIT_FAILURE;
        }

        return EXIT_SUCCESS;
    }

    std::string outpath;
    if (vm.count("output"))
        outpath = vm["output"].as<std::string>();

    try
    {
        parse_osm_file(content, input_file_size, outpath, bulkload);
    }
    catch (const orcus::sax::malformed_xml_error& e)
    {
        orcus::pstring strm(content, input_file_size);
        cerr << orcus::create_parse_error_output(strm, e.offset()) << endl;
        cerr << e.what() << endl;
        return EXIT_FAILURE;
    }
    catch (const std::exception& e)
    {
        cerr << e.what() << endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
