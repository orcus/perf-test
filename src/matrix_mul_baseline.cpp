/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "matrix_mul.hpp"

void matrix_mul_baseline(context& cxt)
{
    const double* left = cxt.input_l.get();
    const double* right = cxt.input_r.get();
    double* output = cxt.output.get();

    for (std::size_t row = 0; row < cxt.size; ++row)
    {
        for (std::size_t col = 0; col < cxt.size; ++col)
        {
            double v = 0.0;

            for (std::size_t pos = 0; pos < cxt.size; ++pos)
                v += left[row * cxt.size + pos] * right[pos * cxt.size + col];

            output[row * cxt.size + col] = v;
        }
    }
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
