/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "matrix_mul.hpp"
#include "env.hpp"

#include <stdexcept>
#include <cstdlib>
#include <iostream>

void matrix_array_deleter::operator()(const double* p)
{
    if (!p)
        return;

#ifdef _WIN32
    _aligned_free((void*)p);
#else
    free((void*)p);
#endif
}

matrix_array_t create_aligned_square_array(
    std::size_t aligned, std::size_t size, bool populate)
{
#ifdef _WIN32
    double* p = (double*)_aligned_malloc(size * size * sizeof(double), aligned);
#else
    double* p = (double*)aligned_alloc(aligned, size * size * sizeof(double));
#endif
    matrix_array_t ret(p);

    auto rem = reinterpret_cast<const uintptr_t>(p) % aligned;

    if (rem)
        throw std::runtime_error("memory is not aligned as specified.");

    if (populate)
    {
        for (std::size_t row = 0; row < size; ++row)
        {
            double* RESTRICT head = p + row * size;
            std::fill_n(head, size, static_cast<double>(row));
        }
    }

    return ret;
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
