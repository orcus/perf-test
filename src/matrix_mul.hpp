/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <cstdint>
#include <vector>
#include <memory>

struct matrix_array_deleter
{
    void operator()(const double* p);
};

using matrix_array_t = std::unique_ptr<double[], matrix_array_deleter>;

struct context
{
    std::size_t alignment;
    std::size_t size; /// size of each side of a square matrix

    matrix_array_t input_l;
    matrix_array_t input_r;
    matrix_array_t output;
};

matrix_array_t create_aligned_square_array(
    std::size_t aligned, std::size_t size, bool populate);

void matrix_mul_baseline(context& cxt);

void matrix_mul_transpose(context& cxt);

void matrix_mul_submatrix(context& cxt);

void matrix_mul_submatrix_sse2(context& cxt);
void matrix_mul_submatrix_avx(context& cxt);

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
