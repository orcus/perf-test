/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "matrix_mul.hpp"
#include "env.hpp"

#include <cstring>
#include <emmintrin.h>
#include <immintrin.h>

namespace {

constexpr std::size_t SM = L1_CACHE_LINESIZE / sizeof(double);

}

void matrix_mul_submatrix(context& cxt)
{
    const double* left = cxt.input_l.get();
    const double* right = cxt.input_r.get();
    double* output = cxt.output.get();

    // Don't forget to zero the output matrix.
    std::memset(output, 0, cxt.size * cxt.size * sizeof(double));

    for (std::size_t row = 0; row < cxt.size; row += SM)
    {
        for (std::size_t col = 0; col < cxt.size; col += SM)
        {
            for (std::size_t pos = 0; pos < cxt.size; pos += SM)
            {
                double* sub_output = &output[row * cxt.size + col]; // top-left of sub-matrix
                const double* sub_left = &left[row * cxt.size + pos];

                // Iterate through the rows of the output and left sub-matrices
                // one row at a time.
                for (std::size_t sub_row = 0; sub_row < SM; ++sub_row, sub_output += cxt.size, sub_left += cxt.size)
                {
                    // sub_output and sub_left shifts by one row at a time.
                    const double* sub_right = &right[pos * cxt.size + col];

                    for (std::size_t sub_pos = 0; sub_pos < SM; ++sub_pos, sub_right += cxt.size)
                    {
                        for (std::size_t sub_col = 0; sub_col < SM; ++sub_col)
                            sub_output[sub_col] += sub_left[sub_pos] * sub_right[sub_col];
                    }
                }
            }
        }
    }
}

void matrix_mul_submatrix_sse2(context& cxt)
{
    const double* RESTRICT left = cxt.input_l.get();
    const double* RESTRICT right = cxt.input_r.get();
    double* RESTRICT output = cxt.output.get();

    // Don't forget to zero the output matrix.
    std::memset(output, 0, cxt.size * cxt.size * sizeof(double));

    for (std::size_t row = 0; row < cxt.size; row += SM)
    {
        for (std::size_t col = 0; col < cxt.size; col += SM)
        {
            for (std::size_t pos = 0; pos < cxt.size; pos += SM)
            {
                double* RESTRICT sub_output = &output[row * cxt.size + col];
                const double* RESTRICT sub_left = &left[row * cxt.size + pos];

                for (std::size_t sub_row = 0; sub_row < SM; ++sub_row, sub_output += cxt.size, sub_left += cxt.size)
                {
                    const double* RESTRICT sub_right = &right[pos * cxt.size + col];
                    // fetch data using non-temporal aligned (NTA) hint i.e.
                    // don't pollute the cache with this data.
                    _mm_prefetch((const char*)&sub_left[SM], _MM_HINT_NTA);

                    for (std::size_t sub_pos = 0; sub_pos < SM; ++sub_pos, sub_right += cxt.size)
                    {
                        // Load the sub-left value (one value) into the lower of m1d, and zero the higher of m1d.
                        __m128d left1 = _mm_load_sd(&sub_left[sub_pos]);
                        // Take the lower half of m1d and duplicate it into lower and higher bits of m1d.
                        // This essentiallly loads the value of sub_left[sub_pos] and duplicates it into two values
                        // in m1d.
                        left1 = _mm_unpacklo_pd(left1, left1);

                        for (std::size_t sub_col = 0; sub_col < SM; sub_col += 2)
                        {
                            // Load 2 double values into m2. It must be 16-byte aligned.  Same goes for r2.
                            __m128d right2 = _mm_load_pd(&sub_right[sub_col]);
                            __m128d out2 = _mm_load_pd(&sub_output[sub_col]);

                            // Calculate output = right * left + output with 2 values at a time.
                            _mm_store_pd(
                                &sub_output[sub_col],
                                _mm_add_pd(_mm_mul_pd(right2, left1), out2)
                            );
                        }
                    }
                }
            }
        }
    }
}

#ifdef __AVX__

void matrix_mul_submatrix_avx(context& cxt)
{
    const double* RESTRICT left = cxt.input_l.get();
    const double* RESTRICT right = cxt.input_r.get();
    double* RESTRICT output = cxt.output.get();

    // Don't forget to zero the output matrix.
    std::memset(output, 0, cxt.size * cxt.size * sizeof(double));

    for (std::size_t row = 0; row < cxt.size; row += SM)
    {
        for (std::size_t col = 0; col < cxt.size; col += SM)
        {
            for (std::size_t pos = 0; pos < cxt.size; pos += SM)
            {
                double* RESTRICT sub_output = &output[row * cxt.size + col];
                const double* RESTRICT sub_left = &left[row * cxt.size + pos];

                for (std::size_t sub_row = 0; sub_row < SM; ++sub_row, sub_output += cxt.size, sub_left += cxt.size)
                {
                    const double* RESTRICT sub_right = &right[pos * cxt.size + col];
                    // fetch data using non-temporal aligned (NTA) hint i.e.
                    // don't pollute the cache with this data.
                    _mm_prefetch((const char*)&sub_left[SM], _MM_HINT_NTA);

                    for (std::size_t sub_pos = 0; sub_pos < SM; ++sub_pos, sub_right += cxt.size)
                    {
                        __m256d left1 = _mm256_set_pd(sub_left[sub_pos], sub_left[sub_pos], sub_left[sub_pos], sub_left[sub_pos]);

                        for (std::size_t sub_col = 0; sub_col < SM; sub_col += 4)
                        {
                            __m256d right4 = _mm256_load_pd(&sub_right[sub_col]);
                            __m256d out4 = _mm256_load_pd(&sub_output[sub_col]);

                            // Calculate output = right * left + output with 4 values at a time.
                            _mm256_store_pd(
                                &sub_output[sub_col],
                                _mm256_add_pd(_mm256_mul_pd(right4, left1), out4)
                            );
                        }
                    }
                }
            }
        }
    }
}

#else

void matrix_mul_submatrix_avx(context& cxt)
{
    throw std::runtime_error("submatrix-avx is not enabled.");
}

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
