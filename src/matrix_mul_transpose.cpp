/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "matrix_mul.hpp"

void matrix_mul_transpose(context& cxt)
{
    const double* left = cxt.input_l.get();
    const double* right = cxt.input_r.get();
    double* output = cxt.output.get();

    auto tmp = create_aligned_square_array(cxt.alignment, cxt.size, false);
    double* tmp_p = tmp.get();

    // transpose the right matrix first.
    for (std::size_t row = 0; row < cxt.size; ++row)
    {
        for (std::size_t col = 0; col < cxt.size; ++col)
            tmp_p[col * cxt.size + row] = right[row * cxt.size + col];
    }

    // matrix multiplication with the transposed right matrix.
    for (std::size_t row = 0; row < cxt.size; ++row)
    {
        for (std::size_t col = 0; col < cxt.size; ++col)
        {
            double v = 0.0;

            for (std::size_t pos = 0; pos < cxt.size; ++pos)
                v += left[row * cxt.size + pos] * tmp_p[row * cxt.size + pos];

            output[row * cxt.size + col] = v;
        }
    }
}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
